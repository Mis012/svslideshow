### ScalableVectorSlideshow

A tool to make SVGs into slideshows, and present those slideshows with a style.  
Amazing if you have artistic skills (but don't worry, I don't either ;), because the whole presentation is just a matter of flying over your (vector) canvas.  
In case some feature is not supported in browsers, you can "export" your svg by converting rescpective objects to paths. I recommend this for fonts, because it gives you an unheard of amount of freedom when selecting the perfect font. Plus noone will find out that you used your own `>ˇ<` and `>°<` because you loved a particular english centric font that much. Also colored background must be faked with a rectangle. (but you can lock layers in inkscape, so that's just a minor inconvenience)

### using

1. make a classy looking svg file containing your presentation. Make sure to avoid using features not supported by the targetted browser. Inkscape + Firefox combo was tested, but anything standards-compliant should work.

2. use index.html to create the slideshow page. basically, you add keyframes through which you will later move in a smootly animated fashion. There is an entry for tweaking transition time in seconds (default is 2). When you're finished, click complete, which will open a prompt() with a pseudo-URL that can temporarily be used to access your result. Use "Ctrl+S" to download the result. You may need to press "retry" (this whole feature is suspicious). Alternatively, copy the source code using browser's debugging tools.

3. put the presentation page and the backend script on a server that supports php. make sure they are correctly linked together (changing the url in the Ajax should be enough)

4. change the password to something only you will know.

5. load the presentation page in your browser, and use your phone with my [presentation_remote app](https://gitlab.com/Mis012/presentation_remote) installed to control the presentation.

6. if everything works, you're ready to present :)

if this didn't help you as much as you would like, feel free to suggest changes ;)

### FAQ

Q: Presentations in browsers are ugly
A: That's not a question, that's a statement. The point being: try F11 for fullscreen. I wouldn't bother with this if that wasn't a thing.

Q: How can I be sure everything looks the same on the projector?
A: I believe you just need to use a screen with the same aspect ratio to prepere the presentation (and always remember F11 ;)
If you're really unsure, try changing your computer's resolution to match the projector's and see if anything looks different. (And tell me if it does, that would need to be fixed of course)

Q: Is this only for programmers?  
A: Not necessairly. You just need to edit the sources to change the location of the php script and the password, just think of it like a config file. Also see below.

Q: How does this work? (high level)
A: There is a CSS class for every slide. It looks like this:
```
.state1 {
	transform: matrix(0.56448, 0, 0, 0.56448, -589.042, -3334.68);
	transition: 2s;
}
```
by "applying" this class to the svg, the transform gets executed over two seconds (by default) or however long you want. If you're curious, look up matrix transforms. It's just math, and you don't *need* to understand it  
This is important, because the creation tool would need to be an order of magnitude more complex to allow editing already completed presentations in a sensible manner. But if you understand this simple concept, you can just open the creation tool again, find the desired position, and copy the matrix(...) thing straight from the status in the right bottom corner. Or edit the time by just rewriting the value.
